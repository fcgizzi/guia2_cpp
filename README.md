# Guía 2 - Puerto Seco

## Problemática

Se pide un progama al que se le pueda asignar una cantidad de pilas y contenedores, siendo así, que a estos mismos se les pueda ingresar valores, como también, eliminar y visualizarlos.


## Compilación y ejecución

Para compilar se debe utilizar la terminal de linux, ingresando `g++ main.cpp Pilas.cpp -o main`, o también, teniendo make instalado previamente, utilizar el comando `make`.

Para ejecutar se accede al programa desde la terminal: `./main`.


## Sobre el programa

Al iniciarse, este solicita la cantidad de pilas y de contenedores con los que se trabajará. esto simulando una especie de matriz, luego saltará un menú, el cual pedirá una respuesta a 3 opciones 
    1 Agregar contenedor
    2 Eliminar contenedor
    3 Ver contenedor
    otro Salir
Cada una de estas retornará posterior a su función al mismo menú para seguir trabajando. El ingreso y eliminado de contenedores se realiza de "abajo hacia arriba" en conteccxto de visualzación de pilas.


## Requisitos

Para el funcionamiento del programa se solicita:

- Un computador
- Sistema operativo con alguna distribución de linux
- Tener instalado Make


## Programa construido con

- Linux
- Geany
- Lenguaje C++


## Autor

Franco Cifuentes Gizzi