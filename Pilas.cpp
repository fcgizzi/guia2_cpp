//libreria
#include <iostream>
using namespace std;
//clase
#include "Pilas.h"

// constructores
Pilas::Pilas(){
  // se definene los atributos
  // máximo y tope de pila
	int max = 0;
	int tope = -1;
	string *pila = NULL;
	bool band = false;
}

// se inicia la matriz
// con máximo
void Pilas::matriz(int max){
	this-> max = max;
	this-> pila = new string[max];
}

// cuando llega a un máximo la pila se considera llena
void Pilas::pila_llena(){
	// +1 es cuando ya supera el tope
	if(this-> tope+1 == this-> max){
		this-> band = true;
	}
	else{
		this-> band = false;
	}
}

// cuando la pila esté vacía se le considerará vacía
void Pilas::pila_vacia(){
	if(this-> tope == -1){
		this-> band = true;
	}
	else{
		this->band = false;
	}
}

// para eliminar un contenedor se verifica si la pila está vacía o no
void Pilas::pop(int can_pila){
	// se llama al método
	pila_vacia();
	// cuando la pila está vacía no hay valor para eliminar
	// cuando no está vacía se elimina lo que hay
	if(this-> band){
		cout << ">> No hay contenedores para eliminar" << endl;
	}
	else{
		this-> pila[can_pila] = "\0";
		this-> tope--;
	}
}
// para agregar se verifica que la pila no esté llena
void Pilas::push(string nombre){
	pila_llena();
	// solo cuando no esté llena se pueden agregar valores
	if(this->band){
		cout << ">> Pila llena, no se pueden agregar más contenedores" << endl;
	}
	else{
		this->tope++;
		this->pila[this->tope] = nombre;
	}
}

// metodos get
string Pilas::get_pila(int j){
	// cuando no hay nada en el contenedor se deja un espacio en blanco
	if(this-> pila[j] == "\0"){
		return "  ";
	}
	return this->pila[j];
}

// se retornan el tope y el máximo
int Pilas::get_tope(){
	return this->tope;
}
int Pilas::get_max(){
	return this->max;
}
