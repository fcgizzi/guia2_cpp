#include <iostream>
using namespace std;

#ifndef PILAS_H
#define PILAS_H

class Pilas {
	// constructores
    private:
        int max = 0;
        int tope = -1;
        string *pila = NULL;
        bool band = false;

    public:
        // constructores
        Pilas();
        Pilas(int max);
        // métodos
        // fucniones para que el tratado con la pila sea correcto
        void matriz(int max);
        void pila_llena();
        void pila_vacia();
        void push(string nombre);
        void pop(int can_pila);
        // métodos get
        string get_pila(int j);
        int get_tope();
        int get_max();
};
#endif
