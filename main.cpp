/*
 * Compilación: $ make
 * Ejecución: $ ./main
 */
//libreria
#include <iostream>
using namespace std;

//clase
#include "Pilas.h"

// función que agrega contenedores a las pilas
void agregar_contenedor(int n, int m, Pilas puerto_seco[]){
	int pila;
	cout << ">> En qué pila desea agregar el contenedor? ";
	cin >> pila;
	string cont;
	if(pila <= m){
		// se pide la pila y el contenedor
		cout << ">> Qué desea agregar? ";
		cin >> cont;
		// se agrega de forma predeterminada
		puerto_seco[pila-1].push(cont);
		cout << ">> Se ha agregado: "<< cont << endl;
	}
	else{
		cout <<">> No existe esa cantidad de pilas" << endl;
		agregar_contenedor(n, m, puerto_seco);
	}
}

// función encargada de eliminar lo que se desee
void eliminar_contenedor(int n, int m, Pilas puerto_seco[]){
	// variables
	string cont;
	int pil;
	int evaluador = 0;
	cout << ">> Ingrese la pila de la que borrará el contenedor: ";
	cin >> pil;
	// como es un indice de matriz se le resta uno
	pil = pil-1;
	cout << ">> Ingrese el contenedor que desea borrar: ";
	cin >> cont;
	// se hace un ciclo for para buscar lo que se desea eliminar
	for(int i=0; i<m; i++){
		// se busca en la pila que se ingresó
		if (i == pil){
			// busca en la columna el valor
			for(int j=0; j<n; j++){
				if(puerto_seco[i].get_pila(j)==cont){
					// el evaluador aumenta ara que solo se elimine 1
					// al encontrarlo se guarda y se elimina
					evaluador = evaluador+1;
					string elim;
					elim = puerto_seco[i].get_pila(j);
					if(evaluador == 1){
						puerto_seco[i].pop(j);
						cout << ">> Se acaba de eliminar el contenedor: " << elim << endl;
					}	
				}
			}
		}
	}
}

// función imprime pilas
void imprimir(int n, int m, Pilas puerto_seco[]){
	// ciclo for que imprime como matriz
	for(int i = n-1; i >= 0; i--){
		for(int j = 0; j < m; j++){
			// llama a la clase donde se encuentra cada dato con su respectiva coordenada
			cout << "\t[" << puerto_seco[j].get_pila(i) << "]";
		}
		cout << "\n";
	}
}

// función menú en la que se dan opciones
void menu(int n, int m, Pilas puerto_seco[]){
	int op;
	cout << "\t ---------------------------" << endl;
	cout << "\t | 1 - Agregar contenedor  |" << endl;
	cout << "\t | 2 - Remover contenedor  |" << endl;
	cout << "\t | 3 - Ver contenedores    |" << endl;
	cout << "\t | otro - Salir 	   |" << endl;
	cout << "\t ---------------------------" << endl;
	cout << ">> ";
	cin >> op;
	
	//opción que se elija llamará alguna función
		if(op==1){
			cout << "\n---------------------------" << endl;
			agregar_contenedor(n, m, puerto_seco);
			menu(n, m, puerto_seco);
		}
		else if(op==2){
			cout << "\n---------------------------" << endl;
			imprimir(n, m, puerto_seco);
			eliminar_contenedor(n, m, puerto_seco);
			menu(n, m, puerto_seco);
		}
		else if(op==3){
			cout << "\n---------------------------" << endl;
			cout << "\t         --PILAS--" << endl;
			imprimir(n, m, puerto_seco);
			menu(n, m, puerto_seco);
		}
		// cualquier caracter fuera de las opciones hará que finalice el programa
		else{
		cout << "|| ADIOS ||" << endl;
	}

}

// función main
int main(){
	//variables fila y columna
	int n, m;
	cout << ">> Ingresar cantidad de contenedores: ";
	cin >> n;
	cout << ">> Ingrese cantidad de pilas: ";
	cin >> m;
	cout << "-------------------------------" << endl;
	// se llama a la clase de pila y se crea una matriz en base a lo ingresado
	Pilas *puerto_seco = new Pilas[m];
	for(int i=0; i<m; i++){
		puerto_seco[i].matriz(n);	
	}
	// se llama al menú de opciones
	cout << ">> Escoja una opción" << endl;
	menu(n, m, puerto_seco);
	
	return 0;
}
